package vegetable;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rushil
 */
public class VegetableSimulation {
    public static void main(String[] args) {
       
        VegetableFactory factory = VegetableFactory.getInstance();
        Vegetable firstCarrot = factory.getVegetable( VegetableTypes.CARROT, "Orange", 3);
        System.out.println("Carrot is ripe? " + firstCarrot.isRipe());
        
        Vegetable secondCarrot = factory.getVegetable( VegetableTypes.CARROT, "Orange", 1.2);
        System.out.println("Carrot is ripe? " + secondCarrot.isRipe());
        
        Vegetable firstBeet = factory.getVegetable( VegetableTypes.BEET, "Red", 3);
        System.out.println("Beet is ripe? " + firstBeet.isRipe());
        
        Vegetable secondBeet = factory.getVegetable( VegetableTypes.BEET, "Red", 1.8);
        System.out.println("Beet is ripe? " + secondBeet.isRipe());
    }
   
}
