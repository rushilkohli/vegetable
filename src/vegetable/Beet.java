package vegetable;

/**
 *
 * @author rushil
 */
public class Beet extends Vegetable{
    private String color;
    private double size;
    
    public Beet(String color, double size) {
       super(VegetableTypes.BEET, color, size);
       this.color=color;
       this.size=size;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public double getSize() {
        return size;
    }

    @Override
    public boolean isRipe() {
        if("Red".equals(getColor()) && getSize() > 2) {
            return true;
        }
        else {
            return false;
        }
    }
    
    @Override
    public String toString() {
        if(isRipe()==true) {
            return ("Beet is Ripe");
        }
        else {
            return("Beet is not Ripe");
        }
    }  
}
