package vegetable;

/**
 *
 * @author rushil
 */
public class Carrot extends Vegetable {
    
    private String color;
    private double size;
    public Carrot(String color, double size) {
        super(VegetableTypes.CARROT,color, size);
        this.color = color;
        this.size = size;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public double getSize() {
        return size;
    }

    @Override
    public boolean isRipe() {
        if("Orange".equals(getColor()) && getSize() > 1.5) {
            return true;
        }
        else {
            return false;
        }
    }
    
    @Override
    public String toString() {
        if(isRipe()==true) {
            return ("Carrot is Ripe");
        }
        else {
            return("Carrot is not Ripe");
        }
    }
}
