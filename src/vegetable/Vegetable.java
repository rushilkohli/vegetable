package vegetable;

/**
 *
 * @author rushil
 */
public abstract class Vegetable {
    private String color;
    private double size;
    private VegetableTypes type;
    
    public Vegetable(VegetableTypes type, String color, double size) {
        this.type = type;
        this.color = color;
        this.size = size;
    }

    public abstract String getColor();
    
    public abstract double getSize();
    
    public abstract boolean isRipe();
    
}
